ubuntu-bionic-mcomix
=========

Ubuntu 18.04 LTS (Bionic Beaver)へUbuntu 18.10(Cosmic  Cuttlefish)にて提供されているMComixをインストールするAnsible Role。

Requirements
------------

### Ansible Version
- Ansible 2.5.2

### Distribution
- Ubuntu 18.04 LTS

### Other Requirements
- Internet Access

Role Variables
--------------

N/A

Dependencies
------------

N/A

Example Playbook
----------------
```
- hosts: bionic
  roles
    - ubuntu-bionic-mcomix
```

License
-------

GPLv3

Author Information
------------------

- Twitter: [@__Kaede](https://twitter.com/__Kaede)
